Some possible outcomes 



- most positive word (based on frequency of words in high-score reviews)
- helpfulness vs text length
- score vs text length
- most positive/negative reviewer (aggregate based on UserId)
- score/text length vs time of the day
- nocturnal reviewers (number of reviews with a userid at night hours)

Take this data and generate more columns (like helpfulness = numerator/denominator, nocturnal = time is night ? true: false, etc.) for all 
interesting stuff. Store them somewhere (maybe the same table), and use some tool that can draw scatter plots and line charts. 

I can think of language (probably useless), sentiment (already done but still has potential), vocabulary
(see how many of the words the reviewer used are in the top 10K/100K/etc. English words lists), grammar nazi-ness 
(there are APIs and some local tools that can give you number of grammatical errors, etc.), similarity of summary and full text.

Rate of obscure words used vs helpfulness score correlated with nocturnal users
